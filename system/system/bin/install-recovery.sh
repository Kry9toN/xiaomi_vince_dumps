#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:23828480:442af59ac39a3b66ca0b959363ebf98a94f37b81; then
  applypatch --bonus /system/etc/recovery-resource.dat \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:14184448:a7737cedbb1f8b22ae028257f21a35da40691cc3 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:23828480:442af59ac39a3b66ca0b959363ebf98a94f37b81 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
